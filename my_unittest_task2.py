import unittest
import task2
class TestMethods(unittest.TestCase):

    def test_test_default(self):
        self.assertEqual(task2.run(), 2)
    def test_test_zero(self):
        self.assertEqual(task2.run("check.txt"), 0)

if __name__ == '__main__':
    unittest.main()