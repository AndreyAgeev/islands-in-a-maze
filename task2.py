import os


PART_OF_ISLAND = 1
NOT_PART_OF_ISLAND = 0


def create_maze(file_name):
    if(os.path.exists(file_name)):
        maze = []
        with open(file_name) as f:
            for line in f:
                maze.append([int(x) for x in line.split()])
    else:
        maze = [[1, 0, 0], [1, 1, 0], [0, 0, 1]]
    return maze


def shape(maze):
    return len(maze), len(maze[0])


def get_coordinates_from_point(point):
    return point[0], point[1]


def check_board(x_pos, y_pos, n_rows, n_cols):
    if y_pos >= 0 and y_pos < n_cols \
      and x_pos >= 0 and x_pos < n_rows:
        return True
    return False


def check_island(maze, point):
    x_pos, y_pos = get_coordinates_from_point(point)
    if(maze[x_pos][y_pos] == PART_OF_ISLAND):
        return True
    return False


def get_point_neighbours(maze, x_pos, y_pos):
    return [(x_pos+1, y_pos), (x_pos-1, y_pos),
            (x_pos, y_pos+1), (x_pos, y_pos-1)]


def get_neighbours(maze, point):
    points = []
    x_pos, y_pos = get_coordinates_from_point(point)
    if check_board(x_pos+1, y_pos, shape(maze)[0], shape(maze)[1]):
        points.append(maze[x_pos+1][y_pos])
    if check_board(x_pos-1, y_pos, shape(maze)[0], shape(maze)[1]):
        points.append(maze[x_pos-1][y_pos])
    if check_board(x_pos, y_pos+1, shape(maze)[0], shape(maze)[1]):
        points.append(maze[x_pos][y_pos+1])
    if check_board(x_pos, y_pos-1, shape(maze)[0], shape(maze)[1]):
        points.append(maze[x_pos][y_pos-1])
    return points


def Island(maze, points):
    for point in points:
        x_pos, y_pos = get_coordinates_from_point(point)
        if(PART_OF_ISLAND in get_neighbours(maze, point)):
            return True
    return False


def destroy_islands(maze, start_x_pos, start_y_pos):
    point = [(start_x_pos, start_y_pos)]
    while Island(maze, point):
        x_pos, y_pos = get_coordinates_from_point(point.pop())
        neighbours = get_point_neighbours(maze, x_pos, y_pos)
        size_x, size_y = shape(maze)
        maze[x_pos][y_pos] = NOT_PART_OF_ISLAND
        for neighbour in neighbours:
            x_pos, y_pos = get_coordinates_from_point(neighbour)
            if not check_board(x_pos, y_pos, size_x, size_y):
                continue
            maze[x_pos][y_pos] = NOT_PART_OF_ISLAND
            point.append((x_pos, y_pos))
    return maze


def find_islands(maze):
    n_rows, n_cols = shape(maze)
    count_islands = 0
    for x_pos in range(0, n_rows):
        for y_pos in range(0, n_cols):
            if maze[x_pos][y_pos] == PART_OF_ISLAND:
                count_islands += 1
                maze = destroy_islands(maze, x_pos, y_pos)
    return count_islands


def run(file_name="default.txt"):
    maze = create_maze(file_name)
    result = find_islands(maze)
    return result


if __name__ == "__main__":
    run()
